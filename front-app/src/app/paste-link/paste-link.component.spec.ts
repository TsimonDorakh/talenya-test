import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasteLinkComponent } from './paste-link.component';
import {YoutubeService} from '../services/youtube.service';
import {CommonSearchService} from '../services/commonSearch.service';
import {MatAutocompleteModule, MatButtonModule, MatInputModule, MatSidenavModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonEmbedService} from '../services/commonEmbed.service';

describe('PasteLinkComponent', () => {
  let component: PasteLinkComponent;
  let fixture: ComponentFixture<PasteLinkComponent>;
  const youtubeService: YoutubeService = jasmine.createSpyObj('youtubeService', ['isValid']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasteLinkComponent ],
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatInputModule, MatSidenavModule, MatButtonModule, MatAutocompleteModule,
      ],
      providers: [CommonEmbedService,
        { provide: YoutubeService, useValue: youtubeService },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasteLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
