import {Component, OnInit} from '@angular/core';
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {AbstractControl, FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {IEmbedService} from "../services/IEmbed.service";
import {CommonEmbedService} from "../services/commonEmbed.service";
import {debounceTime, switchMap} from "rxjs/operators";

function linkValidator(embedService: IEmbedService): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    return embedService.isValid(control.value) ? null : {'link': {value: control.value}};
  };
}

@Component({
  selector: 'app-paste-link',
  templateUrl: './paste-link.component.html',
  styleUrls: ['./paste-link.component.css']
})
export class PasteLinkComponent implements OnInit {
  public safeURL: SafeUrl = null;

  public form: FormGroup = new FormGroup({
    'url': new FormControl(null, [
      linkValidator(this.commonEmbedService),
    ]),
  });

  public get urlFormElement(): AbstractControl {
    return this.form.get('url');
  }

  constructor(private sanitizer: DomSanitizer,
              private commonEmbedService: CommonEmbedService) { }

  ngOnInit(): void {
    this.urlFormElement.valueChanges
      .pipe(debounceTime(500))
      .subscribe(url => {
        this.safeURL = this.commonEmbedService.isValid(url) ?
          this.sanitizer.bypassSecurityTrustResourceUrl(this.commonEmbedService.getEmbedUrl(url)) :
          null;
      });
  }
}
