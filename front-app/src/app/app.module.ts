import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {MatInputModule, MatSidenavModule, MatButtonModule, MatAutocompleteModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { PasteLinkComponent } from './paste-link/paste-link.component';
import { KeywordSearchComponent } from './keyword-search/keyword-search.component';
import {ReactiveFormsModule} from "@angular/forms";
import {YoutubeService} from "./services/youtube.service";
import {CommonEmbedService} from "./services/commonEmbed.service";
import {HttpClientModule} from "@angular/common/http";
import {GoogleApiModule, NG_GAPI_CONFIG} from "ng-gapi";
import {CommonSearchService} from "./services/commonSearch.service";

@NgModule({
  declarations: [
    AppComponent,
    PasteLinkComponent,
    KeywordSearchComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MatInputModule, MatSidenavModule, MatButtonModule, MatAutocompleteModule,
    BrowserAnimationsModule,
    AppRoutingModule, HttpClientModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: {},
    })
  ],
  providers: [YoutubeService, CommonEmbedService, CommonSearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
