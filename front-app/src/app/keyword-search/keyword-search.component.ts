import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {debounceTime, distinctUntilChanged, startWith, switchMap} from "rxjs/operators";
import * as R from "ramda";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {CommonSearchService} from "../services/commonSearch.service";

@Component({
  selector: 'app-keyword-search',
  templateUrl: './keyword-search.component.html',
  styleUrls: ['./keyword-search.component.css']
})
export class KeywordSearchComponent implements OnInit {
  public form: FormGroup = new FormGroup({
    'q': new FormControl(),
  });

  public safeURL: SafeUrl = null;
  public filteredOptions: Observable<Object[]>;

  constructor(private commonSearchService: CommonSearchService,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.filteredOptions = this.form.get('q').valueChanges
      .pipe(
        startWith(null),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(term => {
          if (R.length(term) > 0) {
            return this.commonSearchService.search({term});
          } else {
            return Observable.of([]);
          }
        }),
      );
  }

  public onItemChanged(newItem): void {
    this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl(newItem.url);
  }
}
