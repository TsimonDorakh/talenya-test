import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeywordSearchComponent } from './keyword-search.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule, MatButtonModule, MatInputModule, MatSidenavModule} from '@angular/material';
import {CommonEmbedService} from '../services/commonEmbed.service';
import {CommonSearchService} from '../services/commonSearch.service';
import {YoutubeService} from '../services/youtube.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('KeywordSearchComponent', () => {
  let component: KeywordSearchComponent;
  let fixture: ComponentFixture<KeywordSearchComponent>;
  const youtubeService: YoutubeService = jasmine.createSpyObj('youtubeService', ['isValid']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeywordSearchComponent ],
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatInputModule, MatSidenavModule, MatButtonModule, MatAutocompleteModule,
      ],
      providers: [CommonSearchService,
        { provide: YoutubeService, useValue: youtubeService },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
