import {Observable} from "rxjs/Observable";

export interface ISearchItem {
  title: string;
  url: string;
}

export interface ISearchService {
  search(params: {term: string, limit?: number}): Observable<ISearchItem[]>;
}
