import { Injectable } from '@angular/core';
import {IEmbedService} from "./IEmbed.service";
import {GoogleApiService} from "ng-gapi";
import {Observable} from "rxjs/Observable";
import {fromPromise} from "rxjs/observable/fromPromise";
import {map} from "rxjs/operators";
import * as R from "ramda";
import {ISearchItem, ISearchService} from "./ISearch.service";

@Injectable()
export class YoutubeService implements IEmbedService, ISearchService {
  private regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]{11}).*/;
  private startExp = /t=([0-9]+)/;

  private gapiIsReady: Observable<void>;

  constructor(private gapiService: GoogleApiService) {
    this.initGapi();
  }

  public isValid(url: string): boolean {
    return this.regExp.test(url);
  }

  public getEmbedUrlById(videoId: string): string {
    return `https://www.youtube.com/embed/${videoId}`;
  }

  public getEmbedUrl(url: string): string {
    if (!this.isValid(url)) return null;

    return `https://www.youtube.com/embed/${this.getVideoId(url)}?start=${this.getStartPosition(url)}`;
  }

  public search({term, limit = 5}: {term: string, limit?: number}): Observable<ISearchItem[]> {
    return fromPromise(new Promise(resolve => {
      this.gapiIsReady.subscribe(() => {
        gapi.client.request({
          method: 'GET',
          path: '/youtube/v3/search',
          params: {
            part: 'snippet',
            q: term,
            maxResults: limit,
            type: 'video',
          },
        }).execute(resolve);
      });
    })).pipe(
      map(R.compose(R.map(x => ({
        title: R.path<string>(['snippet', 'title'], x),
        url: this.getEmbedUrlById(R.path(['id', 'videoId'], x)),
      })), R.prop('items'))),
    );
  }

  private getVideoId(url: string): string {
    return url.match(this.regExp)[2];
  }

  private getStartPosition(url: string): string {
    const match = url.match(this.startExp);

    return match ? match[1] : '0';
  }

  private initGapi(): void {
    this.gapiIsReady = fromPromise(new Promise(resolve => {
      this.gapiService.onLoad().subscribe(() => {
        gapi.load('client', () => {
          gapi.client
            .init({apiKey: 'AIzaSyAuT_JILPGaZ3S5X5k-K9FYODWq_GOepag'})
            .then(resolve);
        });
      });
    }));

    this.gapiIsReady.subscribe();
  }
}
