import {IEmbedService} from "./IEmbed.service";
import {Injectable} from "@angular/core";
import {YoutubeService} from "./youtube.service";
import * as R from "ramda";

@Injectable()
export class CommonEmbedService implements IEmbedService {
  private services: IEmbedService[] = [];

  constructor(youtubeService: YoutubeService) {
    this.services.push(youtubeService);
  }

  isValid(url: string): boolean {
    return R.reduce((acc, service) => acc || service.isValid(url), false, this.services);
  }

  getEmbedUrl(url: string): string {
    return R.reduce((acc, service) => {
      return service.isValid(url) ? service.getEmbedUrl(url) : acc;
    }, null, this.services);
  }

}
