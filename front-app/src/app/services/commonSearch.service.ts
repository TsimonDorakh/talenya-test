import {ISearchItem, ISearchService} from "./ISearch.service";
import {Observable} from "rxjs/Observable";
import {YoutubeService} from "./youtube.service";
import {Injectable} from "@angular/core";
import {forkJoin} from "rxjs/observable/forkJoin";
import {map} from "rxjs/operators";
import * as R from "ramda";

@Injectable()
export class CommonSearchService implements ISearchService {
  private services: ISearchService[] = [];

  constructor(youtubeService: YoutubeService) {
    this.services.push(youtubeService);
  }

  search({term, limit = 5}: {term: string, limit?: number}): Observable<ISearchItem[]> {
    const streams = this.services.map(service => service.search({term, limit}));

    return forkJoin(streams).pipe(
      map(R.compose(R.take(limit), R.flatten)),
    );
  }

}
