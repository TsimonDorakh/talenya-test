import { TestBed, inject } from '@angular/core/testing';

import { YoutubeService } from './youtube.service';
import {GoogleApiService} from 'ng-gapi';
import {Observable} from 'rxjs/Observable';
import * as R from "ramda";

describe('YoutubeService', () => {
  const gapi = jasmine.createSpyObj('gapi', ['load']);
  gapi.load = (str, fn) => fn();
  gapi.client = {
    init: R.always(Promise.resolve()),
    request: jasmine.createSpy('request').and.returnValue({execute: fn => fn({items: []})}),
  };

  const gapiService: GoogleApiService = jasmine.createSpyObj('gapiService', ['onLoad']);
  gapiService.onLoad = R.always(Observable.of(null));

  beforeEach(() => {
    window['gapi'] = gapi;

    TestBed.configureTestingModule({
      providers: [YoutubeService,
        { provide: GoogleApiService, useValue: gapiService },
      ],
    });
  });

  it('should be created', inject([YoutubeService], (service: YoutubeService) => {
    expect(service).toBeTruthy();
  }));

  describe('isValid', () => {
    it('should support next test cases', inject([YoutubeService], (service: YoutubeService) => {
      const testCases = [
        { url: 'https://www.youtube.com/watch?v=6QjIHnb5Ivs', expected: true },
        { url: 'https://youtu.be/6QjIHnb5Ivs?t=37', expected: true },
        { url: 'https://www.youtube.com/watch?v=6QjIHnb5Ivs&feature=youtu.be#t=37', expected: true },
        { url: 'https://youtu.be/6QjIHnb5Ivs', expected: true },
        { url: 'https://vimeo.com/146956524', expected: false },
      ];

      testCases.forEach(testCase => {
        expect(service.isValid(testCase.url)).toEqual(testCase.expected);
      });
    }));
  });

  describe('getEmbedUrlById', () => {
    it('should support next test cases', inject([YoutubeService], (service: YoutubeService) => {
      expect(service.getEmbedUrlById('6QjIHnb5Ivs')).toEqual('https://www.youtube.com/embed/6QjIHnb5Ivs');
    }));
  });

  describe('getEmbedUrl', () => {
    it('should return null if url is wrong', inject([YoutubeService], (service: YoutubeService) => {
      expect(service.getEmbedUrl('https://vimeo.com/146956524')).toBeNull();
    }));

    it('should return embeded url', inject([YoutubeService], (service: YoutubeService) => {
      const testCases = [
        { url: 'https://www.youtube.com/watch?v=6QjIHnb5Ivs', expected: 'https://www.youtube.com/embed/6QjIHnb5Ivs?start=0', },
        { url: 'https://youtu.be/6QjIHnb5Ivs?t=37', expected: 'https://www.youtube.com/embed/6QjIHnb5Ivs?start=37' },
        { url: 'https://www.youtube.com/watch?v=6QjIHnb5Ivs&feature=youtu.be#t=37', expected: 'https://www.youtube.com/embed/6QjIHnb5Ivs?start=37' },
        { url: 'https://youtu.be/6QjIHnb5Ivs', expected: 'https://www.youtube.com/embed/6QjIHnb5Ivs?start=0' },
      ];

      testCases.forEach(testCase => {
        expect(service.getEmbedUrl(testCase.url)).toEqual(testCase.expected);
      });
    }));
  });

  describe('search', () => {
    it('should call external service and process the result', inject([YoutubeService], (service: YoutubeService) => {
      gapi.client.request = jasmine.createSpy('request').and.returnValue({execute: fn => fn({items: [
        {id: {videoId: '6QjIHnb5Ivs'}, snippet: {title: 'title1'}},
        {id: {videoId: 'y7tUDs9_gmg'}, snippet: {title: 'title2'}},
      ]})});

      service.search({term: 'video', limit: 10}).subscribe(result => {
        expect(gapi.client.request).toHaveBeenCalledWith({
          method: 'GET',
          path: '/youtube/v3/search',
          params: {
            part: 'snippet',
            q: 'video',
            maxResults: 10,
            type: 'video',
          },
        });

        expect(result).toEqual([
          {title: 'title1', url: 'https://www.youtube.com/embed/6QjIHnb5Ivs'},
          {title: 'title2', url: 'https://www.youtube.com/embed/y7tUDs9_gmg'},
        ]);
      });
    }));
  });

});
