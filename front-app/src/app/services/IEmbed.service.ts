export interface IEmbedService {
  isValid(url: string): boolean;
  getEmbedUrl(url: string): string;
}
