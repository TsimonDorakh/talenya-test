import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PasteLinkComponent} from "./paste-link/paste-link.component";
import {KeywordSearchComponent} from "./keyword-search/keyword-search.component";

const routes: Routes = [
  { path: '', redirectTo: '/paste-link', pathMatch: 'full' },
  { path: 'paste-link', component: PasteLinkComponent },
  { path: 'keyword-search', component: KeywordSearchComponent },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule { }
